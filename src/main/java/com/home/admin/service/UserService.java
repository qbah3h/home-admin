package com.home.admin.service;


import com.home.admin.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.home.admin.controller.dto.UserRegistrationDto;
import org.springframework.stereotype.Service;


public interface UserService extends UserDetailsService {
    User findByEmail(String email);
    User save(UserRegistrationDto registration);
}
