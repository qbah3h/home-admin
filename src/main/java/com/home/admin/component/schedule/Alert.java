package com.home.admin.component.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Alert {
    //  To generate new cron go to http://www.cronmaker.com/
    //  https://docs.oracle.com/cd/E12058_01/doc/doc.1014/e12030/cron_expressions.htm

    //  Fire at 10:15 AM on the 15th day of every month
    @Scheduled(cron = "0 15 10 15 * ?")
    public void createPaymentAlert() {
        String message = "You will be paid soon!";
    }

    //  Executes an alert the second day of every month
    @Scheduled(cron = "0 15 10 15 * ?")
    public void createElectricityAlert() {
        String message = "You have to pay the electricity soon!";
    }

    //  Executes an alert the second day of every month
    @Scheduled(cron = "0 15 10 15 * ?")
    public void createCellphoneAlert() {
        String message = "You have to pay the cell phone soon!";
    }

    //  Executes an alert the second day of every month
    @Scheduled(cron = "0 15 10 15 * ?")
    public void createPhoneAlert() {
        String message = "You have to pay the phone soon!";
    }

    //  Executes an alert the second day of every month
    @Scheduled(cron = "0 15 10 15 * ?")
    public void createRentAlert() {
        String message = "You have to pay the rent soon!";
    }

    //  Executes an alert the 20th day of every month
    @Scheduled(cron = "0 15 10 15 * ?")
    public void createCommonExpensesAlert() {
        String message = "You have to pay the common expenses soon!";
    }

}
