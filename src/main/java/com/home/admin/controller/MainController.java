package com.home.admin.controller;

import com.home.admin.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping({"/index", "/home", "/"})
    public String index() {
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/error")
    public String error() {return "error";}

    @GetMapping("/settings")
    public String settings(Model model) {
        long numberOfUsers = userRepository.count();
        model.addAttribute("numberOfUsers", numberOfUsers);
        return "settings";}
}
