## This is an admin app developed with Spring Boot. 

This README file will be updated every time a new functionality is added.

It is meant that this app can be used to track personal expenses and saves.
More detailed information on how to use it will be available after it is ready.

It has been developed with Spring Boot, Spring Security, Thymeleaf, Bootstrap, JPA
and many of the Spring Boot tools.

The template used was downloaded for free from https://colorlib.com

The app for testing is available on Heroku at https://home-administration.herokuapp.com/
Use the 'admin@admin.com', 'admin' combination to login as Administrator 
and 'user@user.com', 'user' combination to login as User.

Once you create a new user it will be by default USER role.